const express = require("express")
const cors = require("cors")
const port = process.env.PORT || 8080
const connect = require("./utils/mongo").connect
const collection = require("./utils/mongo").collection

const app = express()

app.use(cors())

app.get("/pokemons", function(req, res) {
  console.log(req.query.search)
  const pokemonsCollection = collection("pokemons")

  pokemonsCollection
    .find({
      name: {
        $regex: req.query.search,
        $options: "i" // case insensitive
      }
    })
    .toArray()
    .then(pokemons => res.json(pokemons.map(menuItem => menuItem)))
})

// exports.getMenu = function() {
// return menuCollection
//   .find()
//   .toArray()
//   .then(menuItems =>
//     menuItems.map(menuItem => `${menuItem.name}: $${menuItem.price}\n`)
//   );
// };

// app.listen(port, function() {
//   console.log("Server is listening to port 8080 - juhu");
// });
connect().then(() => {
  console.log("Database connected")
  app.listen(port, function() {
    console.log(`HTTP server listens on http://localhost:${port}`)
  })
})
